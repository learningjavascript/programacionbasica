var teclas = 
{
	UP: 38,
	DOWN: 40,
	LEFT: 37,
	RIGHT: 39
}

console.log(teclas);
document.addEventListener("keydown", dibujarTeclado);
//document.addEventListener("keyup", dibujarTeclado);

var cuadrito = document.getElementById("area_de_dibujo");
var papel = cuadrito.getContext("2d");
var x = 100;
var y = 100;

dibujarLinea("red", x-1, y-1, x+1, y+1, papel);

function dibujarLinea(color, xinicial, yinicial, xfinal, yfinal)
{
	papel.beginPath();
	papel.strokeStyle = color;
	papel.lineWidth = 3;
	papel.moveTo(xinicial, yinicial);
	papel.lineTo(xfinal, yfinal);
	papel.stroke();
	papel.closePath();
}

function dibujarTeclado(evento)
{
	var colorcito = "blue";
	var movimiento = 2;
	switch(evento.keyCode){
		case teclas.UP:
			console.log("arriba");
			dibujarLinea(colorcito, x, y, x, y - movimiento);
			y = y - movimiento;
		break;

		case teclas.DOWN:
			console.log("abajo");
			dibujarLinea(colorcito, x, y, x, y + movimiento);
			y = y + movimiento;
		break;

		case teclas.LEFT:
			console.log("izquierda");
			dibujarLinea(colorcito, x, y, x - movimiento, y);
			x = x - movimiento;
		break;

		case teclas.RIGHT:
			console.log("derecho");
			dibujarLinea(colorcito, x, y, x + movimiento, y);
			x = x + movimiento;
		break;

		default:
			console.log("otra tecla");
	}
}

